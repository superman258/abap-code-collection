class ZCL_BAL definition
  public
  final
  create public .

public section.

  methods CONSTRUCTOR
    importing
      !IV_OBJECT type BALOBJ_D
      !IV_SUBOBJECT type BALSUBOBJ
      !IV_EXTNUMBER type BALNREXT
      !IV_DELDATE type RECADATEFROM optional .
  methods ADD_MSG_WITH_SINGLE_FIELDS
    importing
      !IV_MSGTY type SYMSGTY
      !IV_MSGID type SYMSGID
      !IV_MSGNO type SYMSGNO
      !IV_MSGV1 type CLIKE
      !IV_MSGV2 type CLIKE
      !IV_MSGV3 type CLIKE
      !IV_MSGV4 type CLIKE .
  methods ADD_MSG_WITH_RECAMSG
    importing
      !IS_RECAMSG type RECAMSG .
  methods ADD_MSG_WITH_BAPIRET2
    importing
      !IT_BAPIRET type BAPIRETTAB
      !IS_BAPIRET type BAPIRET2 .
  methods ADD_MSG_WITH_BALMSG
    importing
      !IT_BAL_T_MSG type BAL_T_MSG optional
      !IS_BAL_S_MSG type BAL_S_MSG optional .
  methods ADD_MSG_WITH_SYMSG .
  methods ADD_MSG_WITH_EXCEPTION
    importing
      !IO_EXCEPTION type ref to CX_ROOT .
  methods GET_ALL_MSGS_BAPIRET2
    returning
      value(RT_BAPIRET2) type BAPIRETTAB .
  methods GET_MSGS_FROM_MSGTY_RECAMSG
    importing
      !IV_MSGTY type SYMSGTY
    returning
      value(RT_LIST) type RE_T_MSG .
  methods GET_STATISTICS
    returning
      value(RS_STATISTICS) type BAL_S_SCNT .
  methods DELETE_ALL_COLLECTED_MSGS .
  methods IS_EMPTY .
  methods STORE_AND_FREE
    raising
      CX_OCS_EXCEPTION .
  class-methods ADD_SY_MSG_TO_BALMSG_TAB
    changing
      !XT_BAL_MSG type BAL_T_MSG .
  class-methods FIND_BAL_PROTOCOL
    importing
      !IV_OBJECT type BALOBJ_D
      !IV_SUBOBJECT type BALSUBOBJ
      !IV_EXTNUMBER type BALNREXT
      !IV_DELIVER_NEWEST type RECABOOL default ABAP_FALSE
    returning
      value(RO_INSTANCE) type ref to IF_RECA_MESSAGE_LIST
    exceptions
      ERROR .
  class-methods FIND_BAL_PROTOCOL_BY_HANDLE
    importing
      !IV_HANDLE type BALLOGHNDL
    returning
      value(RO_INSTANCE) type ref to IF_RECA_MESSAGE_LIST
    exceptions
      ERROR .
  class-methods FIND_BAL_LAST_STORED
    importing
      !IV_OBJECT type BALOBJ_D
      !IV_SUBOBJECT type BALSUBOBJ
    returning
      value(RO_INSTANCE) type ref to IF_RECA_MESSAGE_LIST
    exceptions
      ERROR .
protected section.
private section.

  data _GO_BAL type ref to IF_RECA_MESSAGE_LIST .
ENDCLASS.



CLASS ZCL_BAL IMPLEMENTATION.


METHOD add_msg_with_balmsg.

  DATA: ls_recamsg TYPE recamsg.

  LOOP AT it_bal_t_msg ASSIGNING FIELD-SYMBOL(<ls_bal_msg>).
    MOVE-CORRESPONDING <ls_bal_msg> TO ls_recamsg.
    me->_go_bal->add(
      EXPORTING
        is_message       = ls_recamsg
      importing
        es_message       = DATA(ls_message_t)
    ).
  CLEAR ls_recamsg.
  ENDLOOP.

  CLEAR ls_recamsg.
  MOVE-CORRESPONDING is_bal_s_msg TO ls_recamsg.
    me->_go_bal->add(
      EXPORTING
        is_message       = ls_recamsg
      importing
        es_message       = DATA(ls_message_s)
    ).



ENDMETHOD.


METHOD add_msg_with_bapiret2.

  me->_go_bal->add_from_bapi(
    EXPORTING
      it_bapiret     = it_bapiret
      is_bapiret     = is_bapiret
      if_cumulate    = abap_false
    IMPORTING
      ef_add_error   = DATA(lv_add_error)
      ef_add_warning = DATA(lv_add_warning)
  ).

ENDMETHOD.


method ADD_MSG_WITH_EXCEPTION.

 me->_go_bal->add_from_exception(
   EXPORTING
     io_exception = io_exception
 ).

endmethod.


method ADD_MSG_WITH_RECAMSG.

 _go_bal->add(
   EXPORTING
     is_message       = is_recamsg
   IMPORTING
     es_message       = DATA(ls_message)
 ).

endmethod.


METHOD add_msg_with_single_fields.

  me->_go_bal->add( EXPORTING id_msgty = iv_msgty
                              id_msgid = iv_msgid
                              id_msgno = iv_msgno
                              id_msgv1 = iv_msgv1
                              id_msgv2 = iv_msgv2
                              id_msgv3 = iv_msgv3
                              id_msgv4 = iv_msgv4 ).

ENDMETHOD.


METHOD add_msg_with_symsg.

  DATA ls_symsg TYPE recasymsg.

  ls_symsg-msgty = sy-msgty.
  ls_symsg-msgid = sy-msgid.
  ls_symsg-msgno = sy-msgno.
  ls_symsg-msgv1 = sy-msgv1.
  ls_symsg-msgv2 = sy-msgv2.
  ls_symsg-msgv3 = sy-msgv3.
  ls_symsg-msgv4 = sy-msgv4.

  _go_bal->add(
    EXPORTING
      id_msgty     = ls_symsg-msgty
      id_msgid     = ls_symsg-msgid
      id_msgno     = ls_symsg-msgno
      id_msgv1     = ls_symsg-msgv1
      id_msgv2     = ls_symsg-msgv2
      id_msgv3     = ls_symsg-msgv3
      id_msgv4     = ls_symsg-msgv4
*      if_cumulate  = if_cumulate
*      id_detlevel  = id_detlevel
*      id_probclass = id_probclass
*      id_tabname   = id_tabname
*      id_fieldname = id_fieldname
*      id_value     = id_value
*      id_index     = id_index
*      id_intreno   = id_intreno
*      id_custact   = id_custact
*      id_context   = id_context
    IMPORTING
      es_message   = DATA(ls_message)
  ).

ENDMETHOD.


method ADD_SY_MSG_TO_BALMSG_TAB.

  APPEND INITIAL LINE TO xt_bal_msg ASSIGNING FIELD-SYMBOL(<ls_bal_msg>).
  MOVE-CORRESPONDING sy TO <ls_bal_msg>.

endmethod.


METHOD constructor.

  me->_go_bal = cf_reca_message_list=>create( EXPORTING id_object = iv_object
                                                        id_subobject = iv_subobject ).

  me->_go_bal->set_extnumber( EXPORTING id_extnumber = iv_extnumber ).

  DATA: lv_use_deldate_default TYPE recabool.

  lv_use_deldate_default = abap_true.

  IF iv_deldate IS NOT INITIAL.
    lv_use_deldate_default = abap_false.
    DATA(lv_deldate) = iv_deldate.
  ENDIF.
  me->_go_bal->set_expiration_date( EXPORTING if_use_default = lv_use_deldate_default
                                     CHANGING id_expiration_date = lv_deldate ).
ENDMETHOD.


METHOD delete_all_collected_msgs.

 _go_bal->clear( ).

ENDMETHOD.


  METHOD find_bal_last_stored.

    CLEAR ro_instance.

    cf_reca_message_list=>find_last_stored_log(
      EXPORTING
        id_object    = iv_object
        id_subobject = iv_subobject
      RECEIVING
        ro_instance  = ro_instance
      EXCEPTIONS
        error        = 1
        OTHERS       = 2
    ).
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
        WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

  ENDMETHOD.


METHOD find_bal_protocol.

  CLEAR ro_instance.

  cf_reca_message_list=>find(
    EXPORTING
      id_object         = iv_object
      id_subobject      = iv_subobject
      id_extnumber      = iv_extnumber
      if_deliver_newest = abap_false
    RECEIVING
      ro_instance       = ro_instance
    EXCEPTIONS
      error             = 1
      OTHERS            = 2
  ).
  IF sy-subrc <> 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
      WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.

ENDMETHOD.


METHOD find_bal_protocol_by_handle.

  CLEAR ro_instance.

  cf_reca_message_list=>find_by_handle(
    EXPORTING
      id_handle   = iv_handle
    receiving
      ro_instance = ro_instance
    EXCEPTIONS
      error       = 1
      OTHERS      = 2
  ).
  IF sy-subrc <> 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
      WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.

ENDMETHOD.


METHOD get_all_msgs_bapiret2.

 CLEAR rt_bapiret2.

  _go_bal->get_list_as_bapiret(
    IMPORTING
      et_list = rt_bapiret2
  ).

ENDMETHOD.


METHOD get_msgs_from_msgty_recamsg.

  CLEAR rt_list.

  me->_go_bal->get_list(
    EXPORTING
      id_msgty     = iv_msgty
      if_or_higher = abap_true
    IMPORTING
      et_list      = rt_list
  ).

ENDMETHOD.


METHOD get_statistics.

  CLEAR rs_statistics.

  rs_statistics = me->_go_bal->get_statistics( ).

ENDMETHOD.


METHOD is_empty.

  me->_go_bal->is_empty( ).

ENDMETHOD.


METHOD store_and_free.

  IF me->get_all_msgs_bapiret2( ) IS INITIAL.
    RAISE EXCEPTION TYPE cx_ocs_exception MESSAGE w016(/cfg/co_msg).
  ENDIF.

  me->_go_bal->store( ).
  cf_reca_storable=>commit( if_wait = abap_true ).
  me->_go_bal->free( ).

ENDMETHOD.
ENDCLASS.
